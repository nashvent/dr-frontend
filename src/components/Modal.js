// frontend/src/components/Modal.js

import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label
} from "reactstrap";

export default class CustomModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: this.props.activeItem,
      users:this.props.users,
    };
    
  }
  handleChange = e => {
    let { name, value } = e.target;
    if (e.target.type === "checkbox") {
      value = e.target.checked;
    }
    const activeItem = { ...this.state.activeItem, [name]: value };
    this.setState({ activeItem });
  };
  render() {
    const { toggle, onSave } = this.props;
    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}> Nuevo Correo </ModalHeader>
        <ModalBody>
          <Form>
            <FormGroup>
              <Label for="receiver">Correo:</Label>
              <Input
                type="select"
                name="user"
                value={this.state.activeItem.user}
                onChange={this.handleChange}
                placeholder="Correo del receptor">
                  {this.state.users.map(function(user){
                    return <option key={ user.id } value={user.id}>{user.email}</option>;
                  })}
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="subject">Asunto:</Label>
              <Input
                type="text"
                name="subject"
                value={this.state.activeItem.subject}
                onChange={this.handleChange}
                placeholder="Ingresa el asunto"
              />
            </FormGroup>
            <FormGroup>
              <Label for="content">Contenido</Label>
              <Input
                type="textarea"
                name="content"
                value={this.state.activeItem.content}
                onChange={this.handleChange}
                placeholder="Ingresa el contenido"
              />
            </FormGroup>
            
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="success" onClick={() => onSave(this.state.activeItem)}>
            Enviar
          </Button>
        </ModalFooter>
      </Modal>
    );
  }
}
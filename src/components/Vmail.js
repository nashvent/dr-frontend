import React, { Component } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Input,
  Label
} from "reactstrap";

export default class CustomModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: this.props.activeItem
    };
  }
  
  render() {
    const { toggle } = this.props;
    return (
      <Modal isOpen={true} toggle={toggle}>
        <ModalHeader toggle={toggle}>{this.props.activeItem.user.name} {"<"+this.props.activeItem.user.email+">"} </ModalHeader>
        <ModalBody>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><b>Asunto:</b>  {this.state.activeItem.subject}</li>
            <li class="list-group-item">
            <b>
                Mensaje:
                </b>
                <p class="card-text">{this.state.activeItem.content}</p>
            </li>
        </ul>

        </ModalBody>
        <ModalFooter>
          {/* <Button color="success" onClick={() => onSave(this.state.activeItem)}>
            Enviar
          </Button> */}
        </ModalFooter>
      </Modal>
    );
  }
}
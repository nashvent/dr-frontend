 // frontend/src/App.js

 import React, { Component } from "react";
 import Modal from "./components/Modal";
 import Vmail from "./components/Vmail";
 import axios from "axios";

 import {
  Input,
  Form,
  FormGroup
  
} from "reactstrap";

 class App extends Component {
   constructor(props) {
     super(props);
     this.state = {
       viewCompleted: true,
       activeItem: {
         title: "",
         description: "",
       },
       mails:[],
       users:[],
       auser:{
         id:1,
         name:"",
         email:""
       },
       userid:1,
       tempUser:{}
     };
   }
   componentDidMount() {
    this.getUsers();
    this.getUser();
   }
   refreshList = () => {
     axios
       .get("https://anibaldjango.herokuapp.com/api/mails/")
       .then(res => this.setState({ mails: res.data }))
       .catch(err => console.log(err));
   };
   updateUser = () =>{
     console.log("userid",this.state.userid);
     
     var tuser=this.state.users.find(element => element.id == this.state.userid);
     this.setState({auser:tuser})
     //this.getUser();
     this.getMails();
   }
   handleChange = e => {
    let { name, value } = e.target;
    this.state.userid=value;
    this.updateUser();
    //this.setState({ auser: nuser },);
    //;
  };


   getUser = () => {
    axios
      .get("https://anibaldjango.herokuapp.com/api/users/"+this.state.userid)
      .then(res =>{
        this.setState({ auser: res.data });
        
      })
      .catch(err => console.log(err));
    };
   getUsers = () => {
    axios
      .get("https://anibaldjango.herokuapp.com/api/users/")
      .then(res =>{
        this.setState({ users: res.data });
        this.getMails();
      })
      .catch(err => console.log(err));
    };
    getMails = () =>{
      console.log("Var in getMails",this.state.viewCompleted);
      if(this.state.viewCompleted){
          this.getRecdMailsList();
      }
      else{
        this.getSendMailsList();
      }
    }

   getSendMailsList = () => {
    axios
      .get("https://anibaldjango.herokuapp.com/api/user/"+this.state.userid+"/send/")
      .then(res => this.updateMails(res.data.mails))
      .catch(err => console.log(err));
    };

    getRecdMailsList = () => {
      axios
        .get("https://anibaldjango.herokuapp.com/api/user/"+this.state.userid+"/recd/")
        .then(res => this.updateMails(res.data.mails))
        .catch(err => console.log(err));
    };

    updateMails(mails){
      let arr=[];
      for (let entry of mails) {
        var user={};
        if(this.state.viewCompleted){
          user=this.state.users.find(element => element.id == entry.sender_id);
        }
        else{
          user=this.state.users.find(element => element.id == entry.receiver_id);
        }
        entry.user=user;
        arr.push(entry);
      }
      this.setState({ mails: arr }) 
    };

   displayCompleted = status => {
    console.log("Status ant ",status);
    this.state.viewCompleted=status;
    
    this.getMails();
   };
   renderTabList = () => {
     return (
       <div className="my-5 tab-list">
         <span
           onClick={() => this.displayCompleted(true)}
           className={this.state.viewCompleted ? "active" : ""}>
           Recibidos
         </span>
         <span
           onClick={() => this.displayCompleted(false)}
           className={this.state.viewCompleted ? "" : "active"}>
           Enviados
         </span>
       </div>
     );
   };
   renderItems = () => {
     
     var newItems = this.state.mails;
     return this.state.mails.map(item => (
       <tr key={item.id}>
         <td title={item.sender}>
           {item.user.email}
         </td>
         
         <td title={item.subject}>
           {item.subject}
         </td>
         <td>
           <button onClick={() => this.editItem(item)} className="btn btn-secondary mr-2" >Ver</button>
           <button onClick={() => this.handleDelete(item)} className="btn btn-danger">Eliminar</button>
         </td>
       </tr>
     ));
   };
   toggle = () => {
     this.setState({ modal: !this.state.modal });
     console.log("pasando");
   };
   toggle2 = () => {
    this.setState({ vmail: !this.state.vmail });
    console.log("pasando");
  };
   handleSubmit = item => {
     var nitem={
       content:item.content,
       subject:item.subject,
       sender:this.state.auser.id,
       receiver:item.user
     }

     this.toggle();
      console.log("handleSubmit",item);
       axios
      .post("https://anibaldjango.herokuapp.com/api/mails/", nitem)
      .then(this.displayCompleted(false));

     /*axios
       .post("https://anibaldjango.herokuapp.com/api/todos/", item)
       .then(res => this.refreshList());*/
   };
   handleDelete = item => {
     axios
       .delete(`https://anibaldjango.herokuapp.com/api/mails/${item.id}`)
       .then(res => {
          this.getMails();
       });
   };
   createItem = () => {
     const item = { email: "", subject: "", content: "",user:this.state.users[0].id };
     this.setState({ activeItem: item, modal: !this.state.modal });
   };
   editItem = item => {
     this.setState({ activeItem: item, vmail: !this.state.vmail });
   };
  

   /* Parte de la vista  */
   render() {
     return (
       <main className="content">
         <h1 className="text-white text-uppercase text-center my-4">NMail </h1>
         <h3 className="text-white text-uppercase text-center my-4">{this.state.auser.email}</h3>
         <div className="row ">
           <div className="col-md-6 col-sm-10 mx-auto p-0">
           <Form>
            <FormGroup>
         <Input
                type="select"
                name="userid"
                value={this.state.userid}
                onChange={this.handleChange}>
                  {this.state.users.map(function(user){
                    return <option key={ user.id } value={user.id}>{user.email}</option>;
                  })}
          </Input>
          </FormGroup>
          </Form>
             <div className="card p-3">
               <div  className="text-right">
                 <button onClick={this.createItem} className="btn btn-primary ">
                   Escribir nuevo
                 </button>
               </div>
                
               {this.renderTabList()}
               
               <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">Persona</th>
                    <th scope="col">Asunto</th>
                    <th scope="col">Operacion</th>
                  </tr>
                </thead>
                <tbody>
                {this.renderItems()}
                </tbody>
              </table>
             </div>
           </div>
         </div>
         {this.state.modal ? (
           <Modal
             activeItem={this.state.activeItem}
             toggle={this.toggle}
             onSave={this.handleSubmit}
             users={this.state.users}
           />
         ) : null}

          {this.state.vmail ? (
           <Vmail
             activeItem={this.state.activeItem}
             toggle={this.toggle2}
             user={this.state.auser}
           />
         ) : null}
       </main>
     );
   }
 }
 export default App;